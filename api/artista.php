<?php 
  
  header('Access-Control-Allow-Origin: *');
  header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
  header('Content-Type: application/json');
  define('DIR_BASE', rtrim(__DIR__, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR);

  require_once 'Som13Lib.php';

  $slug = isset($_REQUEST['artista']) ? $_REQUEST['artista'] : '';
  if(strlen($slug) <= 0){
    header('HTTP/1.1 404 Internal Server');
    die( json_encode(array('message' => 'Artista não encontrado', 'code' => 404)) );
  }

  $somlib = new Som13Lib();

  $data = $somlib->getPlaylist($slug);

  if($data == null || strlen($data) <= 0){
    header('HTTP/1.1 404 Internal Server');
    die( json_encode(array('message' => 'Nenhuma música encontrada', 'code' => 404)) );
  }
    
  die($data);
  
?>