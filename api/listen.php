<?php 

  header('Access-Control-Allow-Origin: *');
  header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
  header('Access-Control-Allow-Headers: Content-Type');

  #$url = 'music25.som13.com/04748188ef4014293e742fb8af341afd';
  $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';

  if(strlen($id) <= 0) die();

  $dir_cache = 'cache/musicas/';
  if (!file_exists($dir_cache))
      mkdir($dir_cache, 0755, true);

  $url_local = $dir_cache.$id.".mp3";

  if(!file_exists($url_local)){

    $servers = array(20, 25, 26, 27);
    $server = 'music'.$servers[array_rand($servers)];
    $url = 'http://'.$server.'.som13.com/'.md5($id);
    $remote_fp = fopen($url, "r");
    $local_fp = fopen($url_local, "w");
    while ($buf = fread($remote_fp, 1024)) {
      fwrite($local_fp, $buf);
    }
    fclose($remote_fp);
    
    fclose($local_fp);
  }

  $filesize = filesize($url_local);

  header('Content-Type: audio/mpeg');
  header('Content-length: ' . $filesize);
  header("Content-range: 0-{$filesize}/{$filesize}");
  header('Content-Disposition: filename="'.$id.'.mp3');
  header('X-Pad: avoid browser bug');
  header('Cache-Control: no-cache');

  readfile($url_local);

?>