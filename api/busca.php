<?php 
  
  header('Access-Control-Allow-Origin: *');
  header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
  header('Content-Type: application/json');  
  define('DIR_BASE', rtrim(__DIR__, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR);

  require_once 'Som13Lib.php';

  $busca = isset($_REQUEST['busca']) ? $_REQUEST['busca'] : '';
  if(strlen($busca) <= 0){
    header('HTTP/1.1 404 Internal Server');
    die( json_encode(array('message' => 'Busca não encontrada', 'code' => 404)) );
  }

  $somlib = new Som13Lib();

  $data = $somlib->search($busca);

  if($data == null || strlen($data) <= 0){
    header('HTTP/1.1 404 Internal Server');
    die( json_encode(array('message' => 'Nenhum dado encontrado', 'code' => 404)) );
  }
    
  die($data);
  
?>