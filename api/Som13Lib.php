<?php 
require_once 'SlugLib.php';
require_once 'simple_html_dom.php';
set_time_limit(200);

/**
* Classe que monta uma playlist baseado nos streaming do som13
*/
class Som13Lib{

  private $dir_cache;
  private $file_cache;
  private $time_cache;
  
  function __construct($time_cache = 7){
    $this->time_cache = $time_cache; 
  }

  public function getPlaylist($artist){

    if(strlen($artist) <= 0)
      throw new Exception('Artist Not Found!');

    $artist = stripos($artist, ' ') !== false ? SlugLib::toSlug($artist) : $artist;    
    $this->file_cache = $artist.'.json';
    $this->dir_cache = DIR_BASE.'cache/playlists/'.$artist[0].'/'; 

    $data = $this->getCache();
    if($data !== false && $data !== null)
      return $data;

    $data = $this->request('http://som13.com.br/'.$artist, 'POST');
    if($data === false || empty($data))
      throw new Exception('Error on request artist data!');

    #echo $data; die();

    $mt = array();
    $data = stristr($data, 'var ListaDeMusicas');
    preg_match_all('@ListaDeMusicas\[[0-9]+\]\s?=\s?(.[^;]*);@i', $data, $mt);

    $musics = array();

    if(!isset($mt[1])) return $musics;

    foreach ($mt[1] as $music) {

      $music = str_replace(array('"', '\''), '', $music);
      $music = explode('#', $music);
      
      $musics[] = array(
        'id' => $music[0],
        'artista' => $music[3],
        'titulo' => $music[2],
        'album' => $music[4],
        'url' => 'http://'.$this->server().'.som13.com/'.md5($music[0])
      );

    }

    $json = json_encode($musics);
    $this->setCache($json);
    return $json;
      
  }

  public function search($term){

    if(strlen($term) <= 0)
      throw new Exception('Artist Not Found!');

    $slug_term = SlugLib::toSlug($term);
    $this->file_cache = $slug_term.'.json';
    $this->dir_cache = DIR_BASE.'cache/searchs/'.$slug_term[0].'/'; 

    $data = $this->getCache();
    if($data !== false && $data !== null)
      return $data;

    $data = file_get_html('https://busca.som13.com.br/ajax/GetSuggestions/?u='.urlencode($term));
    if($data === false || empty($data))
      throw new Exception('Error on request artist data!');

    #echo $data; die();

    $response = array(
      'artistas' => array(),
      'albuns' => array(),
      'musicas' => array()
    );

    $lis = $data->find('ul.artists li');
    foreach ($lis as $li) {

      preg_match('@[\'"](.*)[\'"]@', $li->find('span.img', 0)->style, $matches);
      $img = '';
      if(count($matches) > 1)
        $img = str_replace('small_thumb', 'thumb', $matches[1]);

      if($li->find('span.text_inner', 0) == null){
        $response['artistas'][] = array(
          'nome' => trim($li->find('span', 1)->plaintext),
          'slug' => substr($li->find('a', 0)->href, 1),
          'img' => $img
        );
      }else{
        $response['albuns'][] = array(
          'nome' => trim($li->find('span.texto', 0)->plaintext),
          'artista' => trim($li->find('span.small', 0)->plaintext),
          'slug' => substr($li->find('a', 0)->href, 1),
          'img' => $img
        );
      }
    }

    $lis = $data->find('ul.music_list li');
    foreach ($lis as $li) {

      $music = explode('#', str_replace(array("addBoxOut('", "');"), '', $li->find('span.add', 0)->onclick));
        
      $response['musicas'][] = array(
        'id' => $music[0],
        'artista' => $music[3],
        'titulo' => $music[2],
        'album' => $music[4],
        'url' => 'http://'.$this->server().'.som13.com/'.md5($music[0])
      );
      
    }

    $json = json_encode($response);
    $this->setCache($json);
    return $json;
      
  }

  public function getCache(){

    $file_cache = $this->dir_cache.$this->file_cache;

    if(file_exists($file_cache)){

      $now = new Datetime();
      $mdf = new Datetime();
      $mdf->setTimestamp(filemtime($file_cache));

      if(static::dayMinDiff($now, $mdf) < $this->time_cache)
        return file_get_contents($file_cache);

    }

    return false;

  }

  public function setCache($data){

    if (!file_exists($this->dir_cache))
      mkdir($this->dir_cache, 0755, true);

    $fl = fopen($this->dir_cache.$this->file_cache, 'w');
    return fwrite($fl, $data);


  }

  private function request($url, $method = 'GET', $data = array()){

    $ch = curl_init();    
    curl_setopt ($ch, CURLOPT_URL, $url);
    curl_setopt ($ch, CURLOPT_POST, $method === 'POST');
    curl_setopt ($ch, CURLOPT_HTTPHEADER, array("X-Requested-With: XMLHttpRequest", 'Accept: */*'));
    curl_setopt ($ch, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
    
    $data = curl_exec ($ch);
    curl_close($ch);
    return $data;
    
  }

  private function server(){    
    $servers = array(20, 25, 26, 27);
    return 'music'.$servers[array_rand($servers)];
  }

  public static function dayMinDiff($d1, $d2, $modulo = false){

    $diff = $d2->diff($d1);
    $ch_dia = $diff->i;
    
    if($diff->d > 0)
      $ch_dia += $diff->d * 24 * 60;
    if($diff->h > 0)
      $ch_dia += $diff->h * 60;
    if($diff->s > 0)
      $ch_dia += $diff->s / 60;

    $ch_dia = ceil($ch_dia);
    if(!$modulo && $diff->invert == 1)
      $ch_dia = $ch_dia * -1;

    return intval($ch_dia);
  }

}