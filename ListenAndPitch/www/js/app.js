var API_URL = 'http://listenandpitch.ml/';

angular.module('ListenAndPitch', ['ionic', 'ListenAndPitch.controllers', 'ListenAndPitch.services', 'ListenAndPitchDB.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }

    if(window.Connection) {
      if(navigator.connection.type == Connection.NONE) {
        $ionicPopup.confirm({
          title: "Internet Disconectada!",
          content: "Conecte-se a internet para utilizar o aplicativo."
        })
        .then(function(result) {
            if(!result) {
              ionic.Platform.exitApp();
            }
        });
      }
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  
  $stateProvider
      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'tpl/menu.html',
        controller: 'AppCtrl'
      })
      .state('app.main', {
        url: '/main',
        views: {
          'menuContent': {
            templateUrl: 'tpl/main.html',
            controller: 'ResultsCtrl'
          }
        }      
      })
      .state('app.search', {
        url: '/busca/:busca/:artista',
        views: {
          'menuContent': {
            templateUrl: 'tpl/main.html',
            controller: 'ResultsCtrl'
          }
        }      
      })
      .state('app.playlist', {
        url: '/playlist',
        views: {
          'menuContent': {
            templateUrl: 'tpl/playlist.html'
          }
        }   
      })
      .state('app.settings', {
        url: '/settings',
        views: {
          'menuContent': {
            templateUrl: 'tpl/settings.html'
          }
        }
      })
      .state('app.musicsdb', {
        url: '/musicsdb',
        cache: false,
        views: {
          'menuContent': {
            templateUrl: 'tpl/musicdb.html',
            controller: 'MusicsDBCtrl'
          }
        }      
      });

  $urlRouterProvider.otherwise('/app/main');    

})

.directive('fileInput', function ($parse) {
    return {
        restrict: "EA",
        template: "<input type='file' ng-transclude />",
        replace: true,          
        transclude: true,
        link: function (scope, element, attrs) {
 
            var modelGet = $parse(attrs.fileInput);
            var modelSet = modelGet.assign;
            var onChange = $parse(attrs.onChange);
 
            var updateModel = function () {
                scope.$apply(function () {
                    modelSet(scope.$parent, element[0].files[0]);
                    onChange(scope.$parent);
                });                    
            };
             
            element.bind('change', updateModel);
        }
    };
})

.directive('trackProgress', function ($parse) {
    return {
        restrict: "EA",
        templateUrl: "trackProgress.html",
        scope: {},
        replace: true,
        link: function(scope, elm, attrs) {
            angular.extend(scope, {
                total: 0,
                progress: 0,
                time: 0,
                setProgress: function(time) {
                    var seconds = 0;
                    if (time.indexOf(':') > 0) {
                        var str = time.split(':');
                        seconds = (Number(str[0]) * 60 + Number(str[1]));
                    }
                    else {
                        seconds = time;
                    }
                    scope.$parent.$eval(attrs.onChange + '(' + seconds + ')');
                }
            });
            scope.$parent.$watch(attrs.progress, function(progress) {
                scope.progress = progress;
                var str = new Date(0,0,0,0,0,progress).toTimeString().split(" ")[0].split(":");
                var time = (Number(str[0]) > 0)?(60 * Number(str[0]) + Number(str[1])):str[1] + ':' + str[2];
                scope.time = time;
            });
        }
    }
})

;
