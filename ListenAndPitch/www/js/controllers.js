angular.module('ListenAndPitch.controllers', [])

.constant('options', {
  range: {
    pitch: 7,
    tempo: 50
  },
  defaults: {
    pitch: 0,
    tempo: 100
  },
  consts: {
    pitchExp: 0.69314718056,
    EOF: 65535
  }
})

.constant('enums', {
  state: {
    init: 0,
    ready: 1,
    playing: 2,
    paused: 3,
    stopped: 4
  }
})

.controller('AppCtrl', ['$scope', '$ionicLoading', '$ionicPopup', '$ionicSideMenuDelegate', 'MusicService', 'DBService', '$window', '$timeout', '$state', '$stateParams', '$interval', '$parse', 'options', 'enums', 'audioContext', 'soundtouch', 
  function ($scope, $ionicLoading, $ionicPopup, $ionicSideMenuDelegate, MusicService, DBService, $window, $timeout, $state, $stateParams, $interval, $parse, options, enums, audioContext, soundtouch) {

  $scope.results = [];
  $scope.empty_results = true;
  $scope.music_playing = null;
  $scope.music_playing_index = 0;
  $scope.playlist = [];
  $scope.db_enable = false;

  DBService.setup().then(function(){
    $scope.db_enable = true;    
  });

  $scope.getMusicPlaylist = function(id){
    for (var i = 0; i < $scope.playlist.length; i++) {
      if($scope.playlist[i].id == id)
        return $scope.playlist[i].id;
    }
    return null;
  }

  $scope.getMusicIndexPlaylist = function(id){
    for (var i = 0; i < $scope.playlist.length; i++) {
      if($scope.playlist[i].id == id)
        return i;
    }
    return -1;
  }

  $scope.toggleLeftSideMenu = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };

  $scope.redirBusca = function(){
    $state.go('app.search', {busca: this.busca, artista: 0})
  }

  $scope.redirArtista = function(sl){
    $state.go('app.search', {busca: sl, artista: 1})
  }

  $scope.playMusic = function(music){
    $scope.addMusic(music);
    $scope.music_playing = music;
  }

  $scope.addMusic = function(music){
    if(angular.isUndefined(music) || angular.isUndefined(music.id) || $scope.getMusicPlaylist(music.id) != null) return;
    music.included_at = new Date().getTime();
    $scope.playlist.push(music);  
  }

  $scope.rmAllMusic = function(){
    $scope.playlist = [];
  }  

  $scope.rmMusic = function(music){      
    if(angular.isUndefined(music) || angular.isUndefined(music.id)) return;

    var ind = $scope.getMusicIndexPlaylist(music.id);
    if(ind >= 0)
       $scope.playlist.splice(ind, 1);  
  }

  $scope.previousMusic = function(){
    if($scope.music_playing_index > 0 && $scope.playlist.length > 1)
      $scope.music_playing_index--;
    $scope.music_playing = $scope.playlist[$scope.music_playing_index];
  }

  $scope.nextMusic = function(){
    if($scope.music_playing_index < ($scope.playlist.length - 1))
      $scope.music_playing_index++;
    $scope.music_playing = $scope.playlist[$scope.music_playing_index];
  }
    
  $scope.$watch('music_playing', function(val){
    
    //$scope.music_playing = val;
    if($scope.music_playing == null) {
      $scope.music_playing_index = 0;
      return;
    }

    $scope.music_playing_index = $scope.getMusicIndexPlaylist($scope.music_playing.id);
    requestDbFile();

  });

  function prepareTrack(buffer) {
    
    //if(!isPlaying()){
      $ionicLoading.show({
        template: "Decodificando arquivo..."
      });
    //}

    audioContext.decodeAudioData(buffer, function(buffer) {
      $scope.$apply(function() {

        if(isPlaying())
          $scope.stopPlaying();

        $scope.audio.buffer = buffer;
        $scope.state.progress = 0;
        $scope.state.duration = buffer.length / buffer.sampleRate;
        $scope.state.status = enums.state.ready;
        

        if(!isPlaying()){
          $scope.toggleState();
        }

        $ionicLoading.hide();

      }); 
    });


  }
  
  //magno
  function requestRemoteFile(){

    if($scope.music_playing == null || angular.isUndefined($scope.music_playing.id) || $scope.music_playing.id.length <= 0) return;

    $ionicLoading.show({
      template: "Baixando música..."
    });
    
    MusicService.listen($scope.music_playing.id).then(function successCallback(response) {
        $ionicLoading.hide();

        if($scope.db_enable){
          var tm = {blob: response.data};
          angular.extend(tm, $scope.music_playing);

          DBService.updateMusic(tm).then(function(){
            prepareTrack(response.data);  
          });
        }else{
          prepareTrack(response.data);  
        }
        
      }, function errorCallback(response) {
        $ionicLoading.hide();        
        $ionicPopup.alert({
          title: 'Erros',
          content: 'Erro ao carregar música'
        });
      });
  } 

  function requestDbFile(){

    if($scope.music_playing == null || angular.isUndefined($scope.music_playing.id) || $scope.music_playing.id.length <= 0) return;

    if(!$scope.db_enable) {
      requestRemoteFile();
      return;
    }

    DBService.getMusicById($scope.music_playing.id).then(function(results) {

        if(angular.isUndefined(results) || results.length <= 0){
          console.log('no results');
          requestRemoteFile();
          return;
        }
        var musicdb = results[0];        
        if(angular.isUndefined(musicdb) || angular.isUndefined(musicdb.blob)){
          console.log('no blob');
          requestRemoteFile();
          return; 
        }

        prepareTrack(musicdb.blob);  
        
    });
    
  } 

  function setDefaultValues() {
    $scope.state.tempo = options.defaults.tempo;
    $scope.state.pitch = options.defaults.pitch;
    $scope.state.progress = 0;
    $scope.state.duration = 0;
  }

  function isPlaying() {
      return $scope.state.status === enums.state.playing;
  };

  angular.extend($scope, {
    options: options,
    enums: enums,
    state: {
      tempo: options.defaults.tempo,
      pitch: options.defaults.pitch,
      duration: 0,
      progress: 0,
      status: enums.state.init
    },
    audio: {
      context: audioContext,
      gainNode: audioContext.createGain()
    },
    getDuration: function(val) {
      return new Date(0,0,0,0,0,val);
    },
    setPitch: function(val, tempo) {
      var isCurrentlyPlaying = isPlaying();
      if (isCurrentlyPlaying)
        $scope.toggleState();
    
      $scope.state.pitch = Math.min(Math.max(val, -(options.range.pitch)), (options.range.pitch));
      $scope.state.tempo = Math.min(Math.max(tempo, options.defaults.tempo - options.range.tempo), options.defaults.tempo + options.range.tempo);
  
      if (isCurrentlyPlaying)
        $scope.toggleState();
    },
    toggleState: function() {      

      if(angular.isUndefined($scope.audio.buffer)){

        if($scope.playlist.length > 0){
          $scope.playMusic($scope.playlist[0]);
        }

        return;
      }

      if ($scope.state.status !== enums.state.playing) {
        $scope.soundtouch = new soundtouch.SoundTouch();
        $scope.audio.source = new soundtouch.WebAudioBufferSource($scope.audio.buffer);
        $scope.audio.filter = new soundtouch.SimpleFilter($scope.audio.source, 
          $scope.soundtouch,
          $scope.state.progress * $scope.audio.buffer.sampleRate);

        var pitch = Math.exp(options.consts.pitchExp * $scope.state.pitch / 12);
        $scope.soundtouch.pitch = pitch;
        $scope.soundtouch.tempo = $scope.state.tempo / 100;
        $scope.state.status = enums.state.playing;

        $scope.audio.node = soundtouch.getWebAudioNode($scope.audio.context, $scope.audio.filter);
        $scope.audio.node.connect($scope.audio.gainNode);
        $scope.audio.gainNode.connect(audioContext.destination);
      }
      else{
        $scope.state.status = enums.state.ready;
        $scope.audio.node.disconnect();
        $scope.audio.gainNode.disconnect();
      }
    },
    seekTo: function(seekTime) {
      $scope.stopPlaying();
      $scope.state.progress = seekTime;
      $scope.toggleState();
    },
    setProgress: function(progress) {
      var isCurrentlyPlaying = isPlaying();
      if (isCurrentlyPlaying)
        $scope.toggleState();
      $scope.state.progress = progress;
      if (isCurrentlyPlaying())
        $scope.toggleState();
    },
    stopPlaying: function() {
      if ($scope.audio.node && $scope.audio.gainNode) {
        $scope.audio.node.disconnect();
        $scope.audio.gainNode.disconnect();
      }
      $scope.state.status = enums.state.ready;
      $scope.state.progress = 0;
    },
    resetParams: function() {
      var isCurrentlyPlaying = isPlaying();
      if (isCurrentlyPlaying)
        $scope.toggleState();
      $scope.setPitch(options.defaults.pitch, options.defaults.tempo);
      if (isCurrentlyPlaying)
        $scope.toggleState();
    },
    readFile: function() {
      if (!$scope.file)
        return;
      $timeout($scope.resetParams);
      $scope.state.status = enums.state.init;
      $ionicLoading.show({
        template: "Loading file..."
      });
      var fileReader = new FileReader();
      fileReader.onload = function(event) {
        prepareTrack(event.target.result);
      };
      $ionicLoading.show({
        template: "Reading file..."
      });
      fileReader.readAsArrayBuffer($scope.file);
    },
    //magno
    //fileUrl: "",
    loadFileUrl: function(){
      requestRemoteFile($scope.state.fileUrl);
    }
  });
  var progressWatch = $interval(function() {

      if (isPlaying()){
        $scope.state.progress = Math.floor(
          ($scope.audio.filter ? $scope.audio.filter.sourcePosition : 0) / 
          ($scope.audio.buffer ? $scope.audio.buffer.sampleRate:1));
      }{

      //console.log($scope.state.progress, $scope.audio.filter.sourcePosition, $scope.audio.buffer.length, options.consts.EOF);  

      if (isPlaying() && !($scope.audio.filter.sourcePosition < $scope.audio.buffer.length - options.consts.EOF))
        if($scope.music_playing_index < $scope.playlist.length){
          $scope.nextMusic();
        }else{
          seekTo(0);
          $scope.stopPlaying();
        }
        
      }
  }.bind(this), 1000);

  $scope.$on('$destroy', function() {
    if (angular.isDefined(progressWatch)) {
      $interval.cancel(progressWatch);
      progressWatch = undefined;
    }
  });
  
  
  $scope.redir = function(loc){
    $state.go(loc);
  }

}])

.controller('ResultsCtrl', ['$scope', '$ionicLoading', '$ionicPopup', 'MusicService', '$state', '$stateParams',
  function ($scope, $ionicLoading, $ionicPopup, MusicService, $state, $stateParams) {    

    $scope.execSearch = function(){

      var busca = $stateParams.busca;

      if(angular.isUndefined(busca) || busca.length <= 0){      
        return;
      }

      $ionicLoading.show({
        template: 'Buscando...'
      });

      MusicService.search(busca).then(
        function(response){
          
          $ionicLoading.hide();
          $scope.results = response.data;

          $scope.empty_results = $scope.results.artistas.length == 0 &&
                                 $scope.results.musicas.length == 0 &&
                                 $scope.results.albuns.length == 0;

        },
        function(response){                  
          
          $ionicLoading.hide();
          $ionicPopup.alert({
            title: 'Erros',
            content: response.data != null && angular.isDefined(response.data.message ) ? response.data.message : "Erro ao executar busca"
          });
          
        });

    }

    $scope.execArtista = function(){

      var artista = $stateParams.busca;

      if(angular.isUndefined(artista) || artista.length <= 0){      
        return;
      }

      $ionicLoading.show({
        template: 'Buscando...'
      });

      MusicService.artista(artista).then(
        function(response){
          
          $scope.results = [];
          $ionicLoading.hide();
          $scope.results.musicas = response.data;

          $scope.empty_results = $scope.results.musicas.length == 0;

        },
        function(response){                  

          $ionicLoading.hide();
          $ionicPopup.alert({
            title: 'Erros',
            content: response.data != null && angular.isDefined(response.data.message ) ? response.data.message : "Erro ao executar busca"
          });
          
        });

    }

    $scope.addAllMusic = function(){
      
      if(angular.isUndefined($scope.results.musicas) || $scope.results.musicas.length <= 0) return;
      
      for (var i = 0; i < $scope.results.musicas.length; i++) {
        $scope.addMusic($scope.results.musicas[i]);
      }  
    }

    if(angular.isDefined($stateParams.busca) && $stateParams.busca.length > 0){    
      $scope.$parent.busca = $stateParams.busca.replace(/\//g, ' ');
      if("1" == $stateParams.artista){
        $scope.execArtista();
      }else{
        $scope.execSearch();
      }
    }

}])

.controller('MusicsDBCtrl', ['$scope', '$ionicLoading', '$ionicPopup', 'MusicService', 'DBService', '$state', '$stateParams',
  function ($scope, $ionicLoading, $ionicPopup, MusicService, DBService, $state, $stateParams) {    

    $scope.results = [];  

    DBService.setup().then(function(){
      DBService.getMusics().then(function(results){        
        $scope.results = results;        
      });
    });

    $scope.rmMusicDb = function(id){

      if(angular.isUndefined(id) || angular.isUndefined($scope.results) || $scope.results.length <= 0) return;

        for (var i = 0; i < $scope.results.length; i++) {
          if(id == $scope.results[i].id){
            DBService.delMusic($scope.results[i].id).then(function(){
              $scope.results.splice(i, 1);    
              console.log($scope.results, i);
            });   
            break;      
          }
        }  
    } 

    $scope.rmAllMusicDb = function(){
      if(angular.isUndefined($scope.results) || $scope.results.length <= 0) return;
      
      for (var i = 0; i < $scope.results.length; i++) {
        $scope.addMusic($scope.results[i]);
      }  
    }

}])

;