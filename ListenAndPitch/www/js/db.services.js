angular.module('ListenAndPitchDB.services', [])

.factory("DBService", function ($q, $ionicPopup, $state, $window) {
    var db = null;

    function createDB() {   

        var deferred = $q.defer();
        if(("indexedDB" in window) == false){
            deferred.reject("indexedDB no support.");
            return deferred.promise;
        }
        var version = 1;
        var request = $window.indexedDB.open("listenAndPitchDB", version);

        request.onupgradeneeded = function(e) {
            db = e.target.result;
            e.target.transaction.onerror = indexedDB.onerror;

            if(db.objectStoreNames.contains("lapData")) {
                db.deleteObjectStore("lapData");
            }

            var store = db.createObjectStore("lapData", { keyPath: "id", autoIncrement:true });
        };

        request.onsuccess = function(e) {
            db = e.target.result;
            deferred.resolve();
        };

        request.onerror = function(e){
            deferred.reject("Error creating database.");
            console.dir(e);
        };

        return deferred.promise;
    }

    function getMusics(){
        var deferred = $q.defer();

        if(db === null){          
            deferred.reject("IndexDB is not opened yet!");
        }
        else{
            var trans = db.transaction(["lapData"], "readwrite");
            var store = trans.objectStore("lapData");
            var musics = [];

            // Get everything in the store;
            var keyRange = IDBKeyRange.lowerBound(0);
            var cursorRequest = store.openCursor(keyRange);

            cursorRequest.onsuccess = function(e) {              
                var result = e.target.result;
                if(result === null || result === undefined){
                    deferred.resolve(musics);
                }else{
                    musics.push(result.value);
                    result.continue();
                }
            };

            cursorRequest.onerror = function(e){                
                deferred.reject("Error retrieving records " + e.value);
            };
        }
        return deferred.promise;
    }

    function getMusicById(musicId){
        var deferred = $q.defer();

        if(db === null){
            deferred.reject("IndexDB is not opened yet!");
        }
        else{
            var transaction = db.transaction(["lapData"], "readwrite");
            var objectStore = transaction.objectStore("lapData");
            var request = objectStore.get(musicId);

            var musics = [];

            request.onsuccess = function(event) {
                var music = request.result;
                musics.push(music);
                deferred.resolve(musics);
            };

            request.onerror = function(e){
                deferred.reject("Error retrieving records " + e.value);
            };
        }
        return deferred.promise;
    }

    function saveMusic(music){

        var deferred = $q.defer();

        if(db === null){
            deferred.reject("IndexDB is not opened yet!");
        }
        else{
            var trans = db.transaction(["lapData"], "readwrite");
            var store = trans.objectStore("lapData");
            var request = store.add(music);

            request.onsuccess = function(e) {
                deferred.resolve();
            };

            request.onerror = function(e) {
                console.log(e.value);
                deferred.reject("Error saving music.");
            };
        }
        return deferred.promise;
    }

    function delMusic(musicId){
        var deferred = $q.defer();

        if(db === null){
            deferred.reject("IndexDB is not opened yet!");
        }
        else{
            var trans = db.transaction(["lapData"], "readwrite");
            var store = trans.objectStore("lapData");

            var request = store.delete(musicId);

            request.onsuccess = function(e) {
                deferred.resolve();
            };

            request.onerror = function(e) {
                console.log(e.value);
                deferred.reject("Error deleting music.");
            };
        }
        return deferred.promise;
    }

    function updateMusic(music){

        var deferred = $q.defer();

        if(db === null){
            deferred.reject("IndexDB is not opened yet!");
        }
        else{
            var trans = db.transaction(["lapData"], "readwrite");
            var store = trans.objectStore("lapData");
            var request = store.put(music);

            request.onsuccess = function(e) {
                deferred.resolve();
            };

            request.onerror = function(e) {
                console.log(e.value);
                deferred.reject("Error updated music.");
            };
        }
        return deferred.promise;
    }

    return {
        setup: function() {
            return createDB();
        },
        getMusics: function(){
            return getMusics();
        },
        getMusicById: function(musicId){
            return getMusicById(musicId);
        },
        saveMusic: function(music){
            return saveMusic(music);
        },
        delMusic: function(musicId){
            return delMusic(musicId);
        },
        updateMusic: function(music){
            return updateMusic(music);
        }
    }

});